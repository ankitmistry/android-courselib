/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.ncert;

import android.app.ExpandableListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.myvision.courselib.db.DB1;
import com.myvision.courselib.db.Database;
import com.myvision.model.Problem;

import java.util.ArrayList;

/**
 *
 * @author ankit
 */
public class NCERTExerciseProblemActivity extends ExpandableListActivity
{
    // Create ArrayList to hold parent Items and Child Items
    private ArrayList<String> parentItems = new ArrayList<String>();
    private ArrayList<Object> childItems = new ArrayList<Object>();

    String chapterid;
    String chapter;
    
    @Override
    public void onCreate(Bundle savedInstanceState) 
    {

        super.onCreate(savedInstanceState);

        Intent i = getIntent();
        chapterid = i.getStringExtra("chapterid");
        chapter = i.getStringExtra("chapter");
        
        String temp = chapter.substring(chapter.indexOf(".")+1);
        setTitle("CourseLib-Class 9 Mathematics "+temp+" NCERT Solution");
        
        // Create Expandable List and set it's properties
        ExpandableListView expandableList = getExpandableListView(); 
        expandableList.setDividerHeight(5);
        expandableList.setGroupIndicator(null);
        expandableList.setClickable(true);

        setExercise();setProblems();
        
        NCERTExpandableAdapter adapter = new NCERTExpandableAdapter(parentItems, childItems);
        adapter.setInflater((LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE), this);
        expandableList.setAdapter(adapter);
        expandableList.setOnChildClickListener(this);
        
    }

    private void setExercise() {
        parentItems = Database.setExercises(chapterid);
    }

    private void setProblems() {
       childItems = Database.setProblems(chapterid);
    }

    
    @Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}
    
}
