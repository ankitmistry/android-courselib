// Copyright 2012 Google Inc. All Rights Reserved.

package com.myvision.courselib.ncert;

/**
 * Static container class for holding a reference to your YouTube Developer Key.
 */
public class YouTubDeveloperKey {

  /**
   * Please replace this with a valid API key which is enabled for the 
   * YouTube Data API v3 service. Go to the 
   * <a href="https://code.google.com/apis/console/">Google APIs Console</a> to
   * register a new developer key.
   */
  public static final String DEVELOPER_KEY = "AIzaSyDRfNikCWN0zy5xJ4I2iWWgwE5z0LKG5Ec";

}
