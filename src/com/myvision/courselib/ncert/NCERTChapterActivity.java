package com.myvision.courselib.ncert;




import com.google.analytics.tracking.android.EasyTracker;
import com.myvision.courselib.general.ChapterListArrayAdapter;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ListView;
import android.widget.Toast;
import android.view.View;
 
public class NCERTChapterActivity extends ListActivity {
 
	static final String[] chapters = new String[]{"1. Number System", "2. Polynomials", "3. Co-ordinate Geometry",
	        "4. A linear equation in two variables",
	        "5. Introduction to Euclid's Geometry", "6. Line and Angles",
	        "7. Triangles", "8. Quadrilateral",
	        "9. Area of Parallelogram and Triangles", "10. Circles", "11. Constructions",
	        "12. Heron's Formula", "13. Surface Area and Volumes", "14. Statistics",
	        "15. Probability"};
 
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
 
		setListAdapter(new ChapterListArrayAdapter(this, chapters)); 
 
	}

	@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
 
		//get selected items
		Integer chapterid = (position + 1);
        if (chapterid == 11) {
            Toast.makeText(v.getContext().getApplicationContext(), "Coming Soon", Toast.LENGTH_LONG).show();
        } else {
            String itemValue = (String) l.getItemAtPosition(position);
            Intent i = new Intent(getApplicationContext(), NCERTExerciseProblemActivity.class);
            i.putExtra("chapterid", chapterid + "");
            i.putExtra("chapter", itemValue);
            startActivity(i);

        }

        
	}

	
	
	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}
	
}