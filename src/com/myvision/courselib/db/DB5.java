/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;

import com.myvision.model.Problem;

/**
 *
 * @author ankit
 */
public class DB5 {

    public static final String CHAPTERID = "5";
    public static final Integer totalEx = 1;

    public static ArrayList<String> setExercises() {
        ArrayList<String> parentItems = new ArrayList<String>();
        for (int i = 1; i <= totalEx; i++) {
            parentItems.add(CHAPTERID + "." + i);
        }
        return parentItems;
    }

    public static ArrayList<Object> setProblems() {

        ArrayList<Object> childItems = new ArrayList<Object>();
        //3.1
        ArrayList<Object> child = new ArrayList<Object>();
        child.add(new Problem("Q1", "JyBNLJR_uRw"));
        child.add(new Problem("Q2", "j5W4EJJ6Kxs"));
        child.add(new Problem("Q3", "tuCuSiBiuJo"));
        child.add(new Problem("Q4", "FIBccFq2OoY"));
        child.add(new Problem("Q5", "11X8t4eKJdM"));
        child.add(new Problem("Q6", "lNCV_FkSSEI"));
        child.add(new Problem("Q7", "QzQs4bLL5uU"));
        childItems.add(child);

        return childItems;
    }
}
