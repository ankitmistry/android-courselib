/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;

import com.myvision.model.Problem;

/**
 *
 * @author ankit
 */
public class DB6 {

    public static final String CHAPTERID = "6";
    public static final Integer totalEx = 3;

    public static ArrayList<String> setExercises() {
        ArrayList<String> parentItems = new ArrayList<String>();
        for (int i = 1; i <= totalEx; i++) {
            parentItems.add(CHAPTERID + "." + i);
        }
        return parentItems;
    }

    public static ArrayList<Object> setProblems() {

        ArrayList<Object> childItems = new ArrayList<Object>();
        //3.1
        ArrayList<Object> child = new ArrayList<Object>();

        child.add(new Problem("Q1", "TCWADvtX7Eg"));
        child.add(new Problem("Q2", "v_1IvT04hvU"));
        child.add(new Problem("Q3", "pyQwScSYHwM"));
        child.add(new Problem("Q4", "CTRPu2pTVoU"));
        child.add(new Problem("Q5", "q8Zy9HFy_J4"));
        child.add(new Problem("Q6", "h3E1Lbj1k9c"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "A8NUJA6pYyE"));
        child.add(new Problem("Q2", "rBLY9niFarE"));
        child.add(new Problem("Q3", "CF-6wsHA-pI"));
        child.add(new Problem("Q4", "HIhWjXCydCw"));
        child.add(new Problem("Q5", "bLQhqVnN_88"));
        child.add(new Problem("Q6", "9dTAsOJ4YX8"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "5-E0CsVkguM"));
        child.add(new Problem("Q2", "07d0ZqOHdR8"));
        child.add(new Problem("Q3", "WzPW5aaZWSQ"));
        child.add(new Problem("Q4", "J_02EECd8_I"));
        child.add(new Problem("Q5", "BY8aoEWvMOk"));
        child.add(new Problem("Q6", "aj0HHpCwzdU"));
        childItems.add(child);

        return childItems;
    }
}
