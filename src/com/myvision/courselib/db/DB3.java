/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;

import com.myvision.model.Problem;

/**
 *
 * @author ankit
 */
public class DB3 {
    
    public static final String CHAPTERID = "3";
    public static final Integer totalEx = 2;
    
    public static ArrayList<String> setExercises() {
        ArrayList<String> parentItems = new ArrayList<String>();
        for (int i = 1; i <= totalEx; i++) {
            parentItems.add(CHAPTERID+"." + i);
        }
        return parentItems;
    }

    public static ArrayList<Object> setProblems() {

        ArrayList<Object> childItems = new ArrayList<Object>();
        //3.1
        ArrayList<Object> child = new ArrayList<Object>();
        child.add(new Problem("Q1", "2yDwoGreJCk"));
        child.add(new Problem("Q2", "yDo7M1nKf60"));
        childItems.add(child);

        // 3.2
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "WcynXEkeVEU"));
        child.add(new Problem("Q2", "8VUXGnbfZQE"));
        childItems.add(child);


        // 3.3
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "wMmaT5CWGkA"));
        child.add(new Problem("Q2", "BKg4poYxHbE"));
        childItems.add(child);

        return childItems;
    }
}
