/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;

import com.myvision.model.Problem;

/**
 *
 * @author ankit
 */
public class DB14 {

    public static final String CHAPTERID = "14";
    public static final Integer totalEx = 4;

    public static ArrayList<String> setExercises() {
        ArrayList<String> parentItems = new ArrayList<String>();
        for (int i = 1; i <= totalEx; i++) {
            parentItems.add(CHAPTERID + "." + i);
        }
        return parentItems;
    }

    public static ArrayList<Object> setProblems() {

        ArrayList<Object> childItems = new ArrayList<Object>();

        ArrayList<Object> child = new ArrayList<Object>();
        child.add(new Problem("Q1", "MS4ZA2T6c5E"));
        child.add(new Problem("Q2", "AJ13WxHzOSw"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "NcjoJ3QnhP0"));
        child.add(new Problem("Q2", "XnaA7sHEz-g"));
        child.add(new Problem("Q3", "MutAPLwx9yw"));
        child.add(new Problem("Q4", "SsNi44FcR8c"));
        child.add(new Problem("Q5", "uNil0h6YNTY"));
        child.add(new Problem("Q6", "PNOJI87_F6A"));
        child.add(new Problem("Q7", "gcyIzE3fhpU"));
        child.add(new Problem("Q8", "lm6qIHKUbMg"));
        child.add(new Problem("Q9", "iGmk6j-rhTA"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "Nty3WN0VGVA"));
        child.add(new Problem("Q2", "7kg7mOBffVc"));
        child.add(new Problem("Q3", "-RexohYzq00"));
        child.add(new Problem("Q4", "7TqXGnQYDXY"));
        child.add(new Problem("Q5", "pqrQT64fFow"));
        child.add(new Problem("Q6", "uICYzhvxG4c"));
        child.add(new Problem("Q7", "ckL_YN84vCY"));
        child.add(new Problem("Q8", "9ZgZQ_fdVUE"));
        child.add(new Problem("Q9", "uqfb3vzvXLQ"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "QkH6KF2eNd0"));
        child.add(new Problem("Q2", "vK3U3YXzVXg"));
        child.add(new Problem("Q3", "VJ3sd1y3Qkg"));
        child.add(new Problem("Q4", "ELr1Yzfen_8"));
        child.add(new Problem("Q5", "D7KAARRwgII"));
        child.add(new Problem("Q6", "vo2hLWChQBo"));
        childItems.add(child);

        return childItems;
    }
}
