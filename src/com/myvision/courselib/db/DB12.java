/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;

import com.myvision.model.Problem;

/**
 *
 * @author ankit
 */
public class DB12 {

    public static final String CHAPTERID = "12";
    public static final Integer totalEx = 2;

    public static ArrayList<String> setExercises() {
        ArrayList<String> parentItems = new ArrayList<String>();
        for (int i = 1; i <= totalEx; i++) {
            parentItems.add(CHAPTERID + "." + i);
        }
        return parentItems;
    }

    public static ArrayList<Object> setProblems() {

        ArrayList<Object> childItems = new ArrayList<Object>();

        ArrayList<Object> child = new ArrayList<Object>();

        child.add(new Problem("Q1", "fq98JOtAG9M"));
        child.add(new Problem("Q2", "DtbjXrUOwE4"));
        child.add(new Problem("Q3", "keDcZQUuENw"));
        child.add(new Problem("Q4", "2XLWLC1vFpU"));
        child.add(new Problem("Q5", "OCvx3FerUcM"));
        child.add(new Problem("Q6", "tjy7-nCDZR0"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "6wLU1lhb2HY"));
        child.add(new Problem("Q2", "gf5IHRhM9g8"));
        child.add(new Problem("Q3", "wkeBnh3n0EE"));
        child.add(new Problem("Q4", "-7ewOqZPzKY"));
        child.add(new Problem("Q5", "UqWYdsmSsBI"));
        child.add(new Problem("Q6", "4t2D2JEbaTg"));
        child.add(new Problem("Q7", "BlMIqwYAgXY"));
        child.add(new Problem("Q8", "pdURF8pSjCw"));
        child.add(new Problem("Q9", "pXphOPpDAG4"));
        childItems.add(child);

        return childItems;
    }
}
