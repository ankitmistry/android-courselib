/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;

import com.myvision.model.Problem;

/**
 *
 * @author ankit
 */
public class DB8 {

    public static final String CHAPTERID = "8";
    public static final Integer totalEx = 2;

    public static ArrayList<String> setExercises() {
        ArrayList<String> parentItems = new ArrayList<String>();
        for (int i = 1; i <= totalEx; i++) {
            parentItems.add(CHAPTERID + "." + i);
        }
        return parentItems;
    }

    public static ArrayList<Object> setProblems() {

        ArrayList<Object> childItems = new ArrayList<Object>();
        //3.1
        ArrayList<Object> child = new ArrayList<Object>();

        child.add(new Problem("Q1", "qEVABZ6XQQM"));
        child.add(new Problem("Q2", "fetxIsypLKM"));
        child.add(new Problem("Q3", "rGp28FD9zu0"));
        child.add(new Problem("Q4", "tAT4KuoXXgU"));
        child.add(new Problem("Q5", "eapye019mfw"));
        child.add(new Problem("Q6", "-HY9F0O2FzA"));
        child.add(new Problem("Q7", "KPuzvrLNV24"));
        child.add(new Problem("Q8", "TTKx2jxpOQg"));
        child.add(new Problem("Q9", "mKV_rm0zCFg"));
        child.add(new Problem("Q10", "DVHmlyxqNQ0"));
        child.add(new Problem("Q11", "KV275qpKbn4"));
        child.add(new Problem("Q12", "0n3VhoI1Tg0"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "hjwLHt0IRfM"));
        child.add(new Problem("Q2", "4uWu49zh8f4"));
        child.add(new Problem("Q3", "2uwQdbu08H4"));
        child.add(new Problem("Q4", "Zp8wqTXpoQg"));
        child.add(new Problem("Q5", "L7fMm-gEmqs"));
        child.add(new Problem("Q6", "aIBcw1vBf-o"));
        child.add(new Problem("Q7", "7Ty7VRbRyRQ"));
        childItems.add(child);

        return childItems;
    }
}
