/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;

import com.myvision.model.Problem;

/**
 *
 * @author ankit
 */
public class DB10 {

    public static final String CHAPTERID = "10";
    public static final Integer totalEx = 5;

    public static ArrayList<String> setExercises() {
        ArrayList<String> parentItems = new ArrayList<String>();
        for (int i = 1; i <= totalEx; i++) {
            parentItems.add(CHAPTERID + "." + i);
        }
        return parentItems;
    }

    public static ArrayList<Object> setProblems() {

        ArrayList<Object> childItems = new ArrayList<Object>();
        //3.1
        ArrayList<Object> child = new ArrayList<Object>();

        child.add(new Problem("Q1", "-zLZomjQRUk"));
        child.add(new Problem("Q2", "OMpGcCZ5Nw8"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "qKfqbdJVN3k"));
        child.add(new Problem("Q2", "UdSubaJQ0t8"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "nH_ThUh1j1g"));
        child.add(new Problem("Q2", "ziR47bbhBW4"));
        child.add(new Problem("Q3", "1adTQhPwVM4"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "mJxaaM1vVAk"));
        child.add(new Problem("Q2", "9TW3Jg8jSJ4"));
        child.add(new Problem("Q3", "58ci7uN_F18"));
        child.add(new Problem("Q4", "1WKE8UknXeQ"));
        child.add(new Problem("Q5", "p_nxuqB1z7M"));
        child.add(new Problem("Q6", "gVAU2NHbPZI"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "U9D_BzXbrbI"));
        child.add(new Problem("Q2", "K4oiWt3jm38"));
        child.add(new Problem("Q3", "KDg-6dDl3X0"));
        child.add(new Problem("Q4", "OP-1MJP1XKo"));
        child.add(new Problem("Q5", "cI_BO9WrtOQ"));
        child.add(new Problem("Q6", "063Gxr33HL0"));
        child.add(new Problem("Q7", "V6aqnRKwZ3A"));
        child.add(new Problem("Q8", "aXoxdlF2GQI"));
        child.add(new Problem("Q9", "QeLEkent0p8"));
        child.add(new Problem("Q10", "fQ9afuEgWYs"));
        child.add(new Problem("Q11", "y2UHDv1upZQ"));
        child.add(new Problem("Q12", "EUFutkCmLa0"));
        childItems.add(child);

        return childItems;
    }
}
