/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;

import com.myvision.model.Problem;

/**
 *
 * @author ankit
 */
public class DB9 {

    public static final String CHAPTERID = "9";
    public static final Integer totalEx = 4;

    public static ArrayList<String> setExercises() {
        ArrayList<String> parentItems = new ArrayList<String>();
        for (int i = 1; i <= totalEx; i++) {
            parentItems.add(CHAPTERID + "." + i);
        }
        return parentItems;
    }

    public static ArrayList<Object> setProblems() {

        ArrayList<Object> childItems = new ArrayList<Object>();
        //3.1
        ArrayList<Object> child = new ArrayList<Object>();


        child.add(new Problem("Q1", "cv2X5HLbvNE"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "ekA5cIaj8kY"));
        child.add(new Problem("Q2", "RkJ2meRFc3o"));
        child.add(new Problem("Q3", "PoPsDamJtJ0"));
        child.add(new Problem("Q4", "ZZrPOMBWwYA"));
        child.add(new Problem("Q5", "Lut5cdYEbhE"));
        child.add(new Problem("Q6", "yeUlRBMrg9A"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "f7p5fiUNNSw"));
        child.add(new Problem("Q2", "cX1yy208pkg"));
        child.add(new Problem("Q3", "ZR4Fp4MBL-k"));
        child.add(new Problem("Q4", "xkoH43tMPYk"));
        child.add(new Problem("Q5", "gO1loPIY3J4"));
        child.add(new Problem("Q6", "aYjpYV9mz3I"));
        child.add(new Problem("Q7", "nyeLX15rWqU"));
        child.add(new Problem("Q8", "mWVS0DdB9CU"));
        child.add(new Problem("Q9", "LA_ExzJbLWM"));
        child.add(new Problem("Q10", "pJ5QyLeHmrg"));
        child.add(new Problem("Q11", "tCrqiwePHHo"));
        child.add(new Problem("Q12", "4-x2jHxtf8c"));
        child.add(new Problem("Q13", "QUj_PEywzvg"));
        child.add(new Problem("Q14", "yJyJ6gVVmw0"));
        child.add(new Problem("Q15", "PLc6vWfcsw8"));
        child.add(new Problem("Q16", "IynaboRcSkg"));
        childItems.add(child);
        
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "cyC_YW_cAJs"));
        child.add(new Problem("Q2", "sJsWvHS3Ou4"));
        child.add(new Problem("Q3", "1qf0BexiE6M"));
        childItems.add(child);

        return childItems;
    }
}
