/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;

import com.myvision.model.Problem;

/**
 * 
 * @author ankit
 */
public class DB13 {

	public static final String CHAPTERID = "13";
	public static final Integer totalEx = 8;

	public static ArrayList<String> setExercises() {
		ArrayList<String> parentItems = new ArrayList<String>();
		for (int i = 1; i <= totalEx; i++) {
			parentItems.add(CHAPTERID + "." + i);
		}
		return parentItems;
	}

	public static ArrayList<Object> setProblems() {

		ArrayList<Object> childItems = new ArrayList<Object>();

		ArrayList<Object> child = new ArrayList<Object>();

		child.add(new Problem("Q1", "7bWAwsVwjUY"));
		child.add(new Problem("Q2", "_SnmYm02CVc"));
		child.add(new Problem("Q3", "8U6ufTqYIXY"));
		child.add(new Problem("Q4", "eONY1zV97XA"));
		child.add(new Problem("Q5", "k6G33w9wXsk"));
		child.add(new Problem("Q6", "YP5S5gAhubU"));
		child.add(new Problem("Q7", "hWLK1s0yWJw"));
		child.add(new Problem("Q8", "UFuiN-iU9h4"));
		childItems.add(child);

		child = new ArrayList<Object>();
		child.add(new Problem("Q1", "T04Zlyf856Q"));
		child.add(new Problem("Q2", "GQ6Pxb7pKD0"));
		child.add(new Problem("Q3", "PAVgj16Rhaw"));
		child.add(new Problem("Q4", "as84CvOdEOQ"));
		child.add(new Problem("Q5", "bvXbEhvkt0c"));
		child.add(new Problem("Q6", "1IDy1-B8BQo"));
		child.add(new Problem("Q7", "XnK8H3j9_kk"));
		child.add(new Problem("Q8", "oqzFscDQ3m4"));
		child.add(new Problem("Q9", "8SsNYr87j1A"));
		child.add(new Problem("Q10", "7nmwMy-FasM"));
		child.add(new Problem("Q11", "CKAxAMrIfh8"));
		childItems.add(child);

		child = new ArrayList<Object>();
		child.add(new Problem("Q1", "7ePNkFW5Zd0"));
		child.add(new Problem("Q2", "VQyJViqJVbU"));
		child.add(new Problem("Q3", "4LKeVPMWxrQ"));
		child.add(new Problem("Q4", "TJjNpWwLaxI"));
		child.add(new Problem("Q5", "l9iVGm2xGVg"));
		child.add(new Problem("Q6", "o2dVDcNZsEo"));
		child.add(new Problem("Q7", "7zY_L59aL5w"));
		child.add(new Problem("Q8", "s5e-bR_vTsQ"));
		childItems.add(child);

		child = new ArrayList<Object>();
		child.add(new Problem("Q1", "ueBWdIt5JBg"));
		child.add(new Problem("Q2", "aFaECbOezJo"));
		child.add(new Problem("Q3", "pYD3ErNb0lc"));
		child.add(new Problem("Q4", "rRh2EFcmvM8"));
		child.add(new Problem("Q5", "solfyayCmfE"));
		child.add(new Problem("Q6", "V2RmlA0AWk8"));
		child.add(new Problem("Q7", "ZEhnj3NA0HI"));
		child.add(new Problem("Q8", "Biqv5vDkz48"));
		child.add(new Problem("Q9", "cR_hEDtUljg"));
		childItems.add(child);

		child = new ArrayList<Object>();
		child.add(new Problem("Q1", "zkgv2SnGlYQ"));
		child.add(new Problem("Q2", "xG_ILoE_uMs"));
		child.add(new Problem("Q3", "qzM5n_pOu_U"));
		child.add(new Problem("Q4", "3NCW7uZA47Y"));
		child.add(new Problem("Q5", "S2WXOQz3-CY"));
		child.add(new Problem("Q6", "bCMIoZPoZWI"));
		child.add(new Problem("Q7", "9cyrGg8McEI"));
		child.add(new Problem("Q8", "C7HqMqhye68"));
		child.add(new Problem("Q9", "suyfzJegb_Y"));
		childItems.add(child);

		child = new ArrayList<Object>();
		child.add(new Problem("Q1", "z4ovGTWlTCs"));
		child.add(new Problem("Q2", "PrfZJ3dvn9I"));
		child.add(new Problem("Q3", "JafqoklNqBI"));
		child.add(new Problem("Q4", "EJJtUOSdInw"));
		child.add(new Problem("Q5", "aB774aUFBoQ"));
		child.add(new Problem("Q6", "VED7MmX9tZw"));
		child.add(new Problem("Q7", "2U7lTfbu8s8"));
		child.add(new Problem("Q8", "MuhY156llCA"));
		childItems.add(child);

		child = new ArrayList<Object>();
		child.add(new Problem("Q1", "-iJP0oNtHeo"));
		child.add(new Problem("Q2", "2yrHwGre0VY"));
		child.add(new Problem("Q3", "vWvAxQNb7wg"));
		child.add(new Problem("Q4", "eqxGpqXeKV8"));
		child.add(new Problem("Q5", "CNSMAq2Kkd4"));
		child.add(new Problem("Q6", "P4koh_2zHWs"));
		child.add(new Problem("Q7", "qLQxUitqnlE"));
		child.add(new Problem("Q8", "YtZY3_KD5Ek"));
		child.add(new Problem("Q9", "hZ9KCPCkU74"));
		childItems.add(child);

		child = new ArrayList<Object>();
		child.add(new Problem("Q1", "NGU9DEGKG00"));
		child.add(new Problem("Q2", "sBObJ9WhUgY"));
		child.add(new Problem("Q3", "3HOOLeDXnW0"));
		child.add(new Problem("Q4", "B2EE0m5WKdo"));
		child.add(new Problem("Q5", "sIs0xUYzOmY"));
		child.add(new Problem("Q6", "AfqKcVbJFGc"));
		child.add(new Problem("Q7", "waWYSvxGZzI"));
		child.add(new Problem("Q8", "cfG47JhDtp0"));
		child.add(new Problem("Q9", "hbICWG6XjMQ"));
		child.add(new Problem("Q10", "eT3xubldOW4"));
		childItems.add(child);
		
		return childItems;
	}
}
