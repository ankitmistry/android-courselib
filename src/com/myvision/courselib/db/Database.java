/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import com.myvision.model.QuizData;

/**
 * 
 * @author ankit
 */
public class Database {

	public static QuizData[] getQuizData(String cid) {
		int chapterid = Integer.parseInt(cid);
		QuizData[] q = null;
		switch (chapterid) {
		case 1:
			q = DB1.getQuizData();
			break;
		default:
			q = DB1.getQuizData();
			break;
		}
		return q;
	}

	public static ArrayList<String> setExercises(String cid) {
		ArrayList<String> parentItems = new ArrayList<String>();
		int chapterid = Integer.parseInt(cid);
		switch (chapterid) {
		case 1:
			parentItems = DB1.getExercises(cid, 6);
			break;
		case 2:
			parentItems = DB1.getExercises(cid, 5);
			break;
		case 3:
			parentItems = DB1.getExercises(cid, 3);
			break;
		case 4:
			parentItems = DB1.getExercises(cid, 4);
			break;
		case 5:
			parentItems = DB1.getExercises(cid, 1);
			break;
		case 6:
			parentItems = DB1.getExercises(cid, 3);
			break;
		case 7:
			parentItems = DB1.getExercises(cid, 4);
			break;
		case 8:
			parentItems = DB1.getExercises(cid, 2);
			break;
		case 9:
			parentItems = DB1.getExercises(cid, 4);
			break;
		case 10:
			parentItems = DB1.getExercises(cid, 5);
			break;
		case 12:
			parentItems = DB1.getExercises(cid, 2);
			break;
		case 13:
			parentItems = DB1.getExercises(cid, 8);
			break;
		case 14:
			parentItems = DB1.getExercises(cid, 4);
			break;
		case 15:
			parentItems = DB1.getExercises(cid, 1);
			break;
		}
		return parentItems;
	}

	public static ArrayList<Object> setProblems(String cid) {
		ArrayList<Object> childItems = new ArrayList<Object>();
		int chapterid = Integer.parseInt(cid);
		switch (chapterid) {
		case 1:
			childItems = DB1.setProblems();
			break;
		case 2:
			childItems = DB2.setProblems();
			break;
		case 3:
			childItems = DB3.setProblems();
			break;
		case 4:
			childItems = DB4.setProblems();
			break;
		case 5:
			childItems = DB5.setProblems();
			break;
		case 6:
			childItems = DB6.setProblems();
			break;
		case 7:
			childItems = DB7.setProblems();
			break;
		case 8:
			childItems = DB8.setProblems();
			break;
		case 9:
			childItems = DB9.setProblems();
			break;
		case 10:
			childItems = DB10.setProblems();
			break;
		case 12:
			childItems = DB12.setProblems();
			break;
		case 13:
			childItems = DB13.setProblems();
			break;
		case 14:
			childItems = DB14.setProblems();
			break;
		case 15:
			childItems = DB15.setProblems();
			break;
		}
		return childItems;
	}
}
