/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;

import com.myvision.model.Problem;

/**
 *
 * @author ankit
 */
public class DB7 {

    public static final String CHAPTERID = "7";
    public static final Integer totalEx = 4;

    public static ArrayList<String> setExercises() {
        ArrayList<String> parentItems = new ArrayList<String>();
        for (int i = 1; i <= totalEx; i++) {
            parentItems.add(CHAPTERID + "." + i);
        }
        return parentItems;
    }

    public static ArrayList<Object> setProblems() {

        ArrayList<Object> childItems = new ArrayList<Object>();
        //3.1
        ArrayList<Object> child = new ArrayList<Object>();

        child.add(new Problem("Q1", "dpOoVkSaOIM"));
        child.add(new Problem("Q2", "dM66c5NJjJA"));
        child.add(new Problem("Q3", "33JJPvv7s78"));
        child.add(new Problem("Q4", "d11lMHrPugo"));
        child.add(new Problem("Q5", "aruXfuKotNk"));
        child.add(new Problem("Q6", "xtzGD20ZdOU"));
        child.add(new Problem("Q7", "1qhNR0Ykq5c"));
        child.add(new Problem("Q8", "52T5dTU4v7g"));
        childItems.add(child);

        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "UGz0_yyR_2M"));
        child.add(new Problem("Q2", "IZc3cPZLzaQ"));
        child.add(new Problem("Q3", "qBrbcr4yl80"));
        child.add(new Problem("Q4", "4EyXnWoh7lw"));
        child.add(new Problem("Q5", "rysHOqgpMuo"));
        child.add(new Problem("Q6", "YbEO3nGhfJ8"));
        child.add(new Problem("Q7", "neQhNIIHboQ"));
        child.add(new Problem("Q8", "vEQGn2fbXjc"));
        childItems.add(child);

        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "-a5-ntwv8JQ"));
        child.add(new Problem("Q2", "pgM9q99gBa0"));
        child.add(new Problem("Q3", "3nKfp3Ft9o0"));
        child.add(new Problem("Q4", "cELOoTg1z-8"));
        child.add(new Problem("Q5", "0mM1Y4uXdfk"));
        childItems.add(child);

        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "X8Q6006bnX8"));
        child.add(new Problem("Q2", "z9H8UcJAbsU"));
        child.add(new Problem("Q3", "ll22xXzyHjg"));
        child.add(new Problem("Q4", "5ijgeIqt4IA"));
        child.add(new Problem("Q5", "4Z0N_BNpgeA"));
        child.add(new Problem("Q6", "cT29jvaK57k"));
        childItems.add(child);

        return childItems;
    }
}
