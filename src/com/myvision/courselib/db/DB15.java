/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;

import com.myvision.model.Problem;

/**
 *
 * @author ankit
 */
public class DB15 {

    public static final String CHAPTERID = "15";
    public static final Integer totalEx = 1;

    public static ArrayList<String> setExercises() {
        ArrayList<String> parentItems = new ArrayList<String>();
        for (int i = 1; i <= totalEx; i++) {
            parentItems.add(CHAPTERID + "." + i);
        }
        return parentItems;
    }

    public static ArrayList<Object> setProblems() {

        ArrayList<Object> childItems = new ArrayList<Object>();

        ArrayList<Object> child = new ArrayList<Object>();
        child.add(new Problem("Q1", "G_sqiH_DAc8"));
        child.add(new Problem("Q2", "foBsLIG44SU"));
        child.add(new Problem("Q3", "mhUkZxOMkk4"));
        child.add(new Problem("Q4", "OVBpAiZTNak"));
        child.add(new Problem("Q5", "Due6BoYzdpE"));
        child.add(new Problem("Q6", "DCuCZy0mekI"));
        child.add(new Problem("Q7", "7FOHPS9b4rM"));
        child.add(new Problem("Q8", "DCEG5gi2RY4"));
        child.add(new Problem("Q11", "q4cP3joJSMg"));
        child.add(new Problem("Q12", "19pxTI26eLg"));
        child.add(new Problem("Q13", "1QtUMOEa1Vg"));
        childItems.add(child);

        return childItems;
    }
}
