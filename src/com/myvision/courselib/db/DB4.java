/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import java.util.ArrayList;

import com.myvision.model.Problem;

/**
 *
 * @author ankit
 */
public class DB4 {

    public static final String CHAPTERID = "4";
    public static final Integer totalEx = 4;

    public static ArrayList<String> setExercises() {
        ArrayList<String> parentItems = new ArrayList<String>();
        for (int i = 1; i <= totalEx; i++) {
            parentItems.add(CHAPTERID + "." + i);
        }
        return parentItems;
    }

    public static ArrayList<Object> setProblems() {

        ArrayList<Object> childItems = new ArrayList<Object>();
        //3.1
        ArrayList<Object> child = new ArrayList<Object>();

        child.add(new Problem("Q1", "wTzKS3FkJDY"));
        child.add(new Problem("Q2", "bIEN3NlgCnE"));
        childItems.add(child);

        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "4f9zDagBSDI"));
        child.add(new Problem("Q2", "653xD1lZs-8"));
        child.add(new Problem("Q3", "snWKLxF-py8"));
        child.add(new Problem("Q4", "ODhb5L_MUdo"));
        childItems.add(child);

        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "x7WkACqGqag"));
        child.add(new Problem("Q2", "Uq1G_W7G8MQ"));
        child.add(new Problem("Q3", "ZzllNJD_BNQ"));
        child.add(new Problem("Q4", "0S2SSrvh6ME"));
        child.add(new Problem("Q5", "mIgiLmcViEA"));
        child.add(new Problem("Q6", "VkRwKDYXo4o"));
        child.add(new Problem("Q7", "mLvmMahm__0"));
        child.add(new Problem("Q8", "BvKnOq_1uQM"));
        childItems.add(child);

        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "O6y_WHlDDBo"));
        child.add(new Problem("Q2", "ZbquGsdWV5Y"));
        childItems.add(child);

        return childItems;
    }
}
