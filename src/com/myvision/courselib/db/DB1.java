/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.db;

import static com.myvision.courselib.db.DB3.CHAPTERID;
import static com.myvision.courselib.db.DB3.totalEx;
import java.util.ArrayList;

import com.myvision.model.Problem;
import com.myvision.model.QuizData;

/**
 *
 * @author ankit
 */
public class DB1 {
    
    
	public static final String exe = "Exercise ";
	public static final String QUIZ = "Quiz-";
	
    public static ArrayList<String> getExercises(String CHAPTERID,Integer totalEx) {
        ArrayList<String> parentItems = new ArrayList<String>();
        for (int i = 1; i <= totalEx; i++) {
            parentItems.add(exe + " "+ CHAPTERID+"." + i);
        }
        return parentItems;
    }
    
    
    
    
    public static ArrayList<Object> setProblems() 
    {

        ArrayList<Object> childItems = new ArrayList<Object>();
        //1.1
        ArrayList<Object> child = new ArrayList<Object>();
        child.add(new Problem("Q1", "ZRQc3DHJ5NA"));
        child.add(new Problem("Q2", "La5830t2Diw"));
        child.add(new Problem("Q3", "k_VlNGSSBU4"));
        child.add(new Problem("Q4", "zx3xMbuvj5s"));
        childItems.add(child);
        
        // 1.2
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "LSwGB_l1M2U"));
        child.add(new Problem("Q2", "31lj88rVlZY"));
        child.add(new Problem("Q3", "GiRP_87Sgks"));
        childItems.add(child);

        
        // 1.3
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "hSh1btzyLZg"));
        child.add(new Problem("Q3", "SAYxLFrBME0"));
        child.add(new Problem("Q4", "VbSw5uho95M"));
        child.add(new Problem("Q5", "Zo6uwq4xMrI"));
        child.add(new Problem("Q6", "tXqk1XS49Y4"));
        child.add(new Problem("Q7", "HHKxojkKHHg"));
        child.add(new Problem("Q8", "S8T92_coeWg"));
        child.add(new Problem("Q9", "m25C4QurgEw"));
        childItems.add(child);

        // 1.4
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "00h5PzLhbdk"));
        child.add(new Problem("Q2", "qcT1OsQGcpE"));
        childItems.add(child);
        
        // 1.5
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "Wf03Y40T1kY"));
        child.add(new Problem("Q2", "wpTmGtxu2tA"));
        child.add(new Problem("Q3", "sFz9A8G9UBI"));
        child.add(new Problem("Q4", "9ho9NzjUpOQ"));
        child.add(new Problem("Q5", "bj2RxRmLe24"));
        childItems.add(child);
        
        // 1.6
        child = new ArrayList<Object>();
        child.add(new Problem("Q1", "InoD96loLlg"));
        child.add(new Problem("Q2", "IalAjgwcHnk"));
        child.add(new Problem("Q3", "UEKcYA6Cng0"));
        childItems.add(child);
        
        return childItems;
    }
    
    public static QuizData[] getQuizData(){
    	QuizData[] q = new QuizData[6];
    	q[0] = new QuizData("1", QUIZ+"1", "90101");
    	q[1] = new QuizData("2", QUIZ+"2", "90102");
    	q[2] = new QuizData("3", QUIZ+"3", "90103");
    	q[3] = new QuizData("4", QUIZ+"4", "90104");
    	q[4] = new QuizData("5", QUIZ+"5", "90105");
    	q[5] = new QuizData("6", QUIZ+"6", "90106");
    	return q;
    }
    
    
}
