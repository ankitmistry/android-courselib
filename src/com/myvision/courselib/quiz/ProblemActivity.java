package com.myvision.courselib.quiz;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;
import com.myvision.courselib.R;
import com.myvision.courselib.ncert.NCERTChapterActivity;

public class ProblemActivity extends Activity {

    Button back;
    String chapterid;
    String chapter;
    private static final String VIDEO_ID = "k_VlNGSSBU4";
    WebView wv;
    RelativeLayout rl;
    Button tv;
    
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout);

        Intent i = getIntent();
        chapterid = i.getStringExtra("chapterid");
        chapter = i.getStringExtra("chapter");


        //wv = (WebView) findViewById(R.id.webView1);
        //wv.getSettings().setJavaScriptEnabled(true);
        //wv.loadData("<iframe width=\"420\" height=\"315\" src=\"http://www.youtube.com/embed/fetxIsypLKM\" frameborder=\"0\" allowfullscreen></iframe>", "text/html", "utf-8");
        //wv.loadUrl("http://www.google.com");


        /*rl = (RelativeLayout) findViewById(R.id.problem);
        String[] ids = new String[]{"1", "2", "3", "4", "5"};
        for (int item = 0; item < ids.length; item++) {
            TextView textView = new TextView(this);
            // careful id value should be a positive number.
            textView.setText(ids[item]);
            textView.setId(Integer.parseInt(ids[item]));
            rl.addView(textView); 
        }*/
        
        tv = (Button) findViewById(R.id.tv);
        tv.setOnClickListener(new OnClickListener() {

            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "text clicked", Toast.LENGTH_LONG).show();
                
            }
        });
        

        //Toast.makeText(getApplicationContext(), chapter + "," + chapterid, Toast.LENGTH_LONG).show();

        back = (Button) findViewById(R.id.back);
        back.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), NCERTChapterActivity.class);
                startActivity(i);
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.problem, menu);
        return true;
    }
    
    
    @Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}
}
