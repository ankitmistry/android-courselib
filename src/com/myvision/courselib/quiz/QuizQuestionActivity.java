package com.myvision.courselib.quiz;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;
import com.myvision.courselib.R;
import com.myvision.courselib.general.JavaScriptWebAppInterface;
import com.myvision.courselib.general.SimpleCrypto;
import com.myvision.model.Question;
import com.myvision.model.Quiz;

import android.os.Bundle;
import android.os.Environment;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.graphics.SumPathEffect;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.Toast;

@SuppressLint("JavascriptInterface")
public class QuizQuestionActivity extends Activity {

	public static Quiz q;
	public static int totalQues;
	public static Question current;
	public static int currentQuesNo = -1;

	Button Next;
	Button Previous;
	Button Begin;

	@SuppressLint("JavascriptInterface")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quiz_question);
		setTitle("Quiz Activity");

		Intent i = getIntent();
		String jsonFile = i.getStringExtra("jsonfile");

		currentQuesNo = -1;
		WebView wv = (WebView) findViewById(R.id.quiz_wv);
		wv.getSettings().setJavaScriptEnabled(true);
		wv.setWebChromeClient(new WebChromeClient());
		wv.addJavascriptInterface(new JavaScriptWebAppInterface(this),
				"Android");

		wv.loadDataWithBaseURL(
				"http://bar",
				"<script type='text/x-mathjax-config'>"
						+ "MathJax.Hub.Config({ "
						+ "showMathMenu: false, "
						+ "tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]},"
						+ "jax: ['input/TeX','output/HTML-CSS'], "
						+ "extensions: ['tex2jax.js'], "
						+ "TeX: { extensions: ['AMSmath.js','AMSsymbols.js',"
						+ "'noErrors.js','noUndefined.js'] } "
						+ "});</script>"
						+ "<script type='text/javascript' "
						+ "src='file:///android_asset/MathJax/MathJax.js'"
						+ "></script><script type='text/javascript' "
						+ "src='file:///android_asset/jquery.min.js'"
						+ "></script><link rel=\"stylesheet\" href=\"file:///android_asset/my.css\">"
						+ "<script>" + "</script><div id='math'></div>",
				"text/html", "utf-8", "");

		String json = readFromFile(jsonFile);
		// String json = "";

		/*
		 * try { json = SimpleCrypto.decrypt("abc", en); } catch (Exception e1)
		 * { // TODO Auto-generated catch block e1.printStackTrace();
		 * Log.i("dec", json); }
		 */

		q = new Quiz();
		try {
			q = parseJson(json);
		} catch (JSONException e) {
			e.printStackTrace();
		}
		totalQues = q.getQuestions().length;

		Next = (Button) findViewById(R.id.Next);
		Previous = (Button) findViewById(R.id.Previous);
		Begin = (Button) findViewById(R.id.begin);

		Begin.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				// TODO Auto-generated method stub
				if (Begin.getText().equals("Begin")) {
					currentQuesNo++;
					current = q.getQuestions()[currentQuesNo];
					loadNextQuestion();
					Begin.setText("Finish");
					Next.setVisibility(Next.VISIBLE);
					Previous.setVisibility(Previous.VISIBLE);
				} else {
					String opt = "";
					for (int i = 0; i < q.questions.length; i++) {
						opt = opt + q.questions[i].getCorrectAnsCode() + ",";
					}
					// Toast.makeText(getApplicationContext(), opt,
					// Toast.LENGTH_LONG).show();
					checkCorrectAnswer();

				}
			}

			private void checkCorrectAnswer() {
				// TODO Auto-generated method stub
				int correct = 0;
				int wrong = 0;
				for (int i = 0; i < q.questions.length; i++) {
					if (q.questions[i].getUserAnsCode().equals(
							q.questions[i].getCorrectAnsCode())) {
						correct++;
					} else {
						wrong++;
					}
				}
				// Toast.makeText(getApplicationContext(), correct+","+wrong,
				// Toast.LENGTH_LONG).show();
				Intent i = new Intent(getApplicationContext(),
						QuizAnalysisActivity.class);
				i.putExtra("correct", correct + "");
				i.putExtra("wrong", wrong + "");
				startActivity(i);

			}
		});

		Next.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				currentQuesNo++;
				if (currentQuesNo < totalQues) {
					current = q.getQuestions()[currentQuesNo];
					loadNextQuestion();
					Next.setVisibility(Next.VISIBLE);
					Previous.setVisibility(Previous.VISIBLE);
				}
				if (currentQuesNo == totalQues - 1) {
					Next.setVisibility(Next.INVISIBLE);
					Previous.setVisibility(Previous.VISIBLE);
				}

			}
		});

		Previous.setOnClickListener(new OnClickListener() {

			public void onClick(View v) {
				currentQuesNo--;
				if (currentQuesNo > -1) {
					current = q.getQuestions()[currentQuesNo];
					loadNextQuestion();
					Next.setVisibility(Next.VISIBLE);
					Previous.setVisibility(Previous.VISIBLE);
				}
				if (currentQuesNo == 0) {
					Previous.setVisibility(Previous.INVISIBLE);
					Next.setVisibility(Next.VISIBLE);
				}

			}
		});

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.quiz, menu);
		return true;
	}

	public void loadNextQuestion() {

		WebView wv = (WebView) findViewById(R.id.quiz_wv);
		wv.setWebChromeClient(new WebChromeClient());
		wv.addJavascriptInterface(new JavaScriptWebAppInterface(
				getApplicationContext()), "Android");

		String[] opt = current.getOption().split(",");

		ArrayList<Integer> optIndex;
		if (current.shuffled == false) {
			optIndex = shuffle();
			current.setOptShuffleOrder(optIndex);
			current.setShuffled(true);
		} else {
			optIndex = current.getOptShuffleOrder();
		}

		String option = "<table id=\"opt\"><tbody>"
				+ "<tr><td class=\"radio\"><input id=\"r"
				+ optIndex.get(0)
				+ "\" name=\"userAns\" value=\""
				+ optIndex.get(0)
				+ "\" type=\"radio\"></td><td class=\"option\" id=\"o"
				+ optIndex.get(0)
				+ "\" onclick=\"document.getElementById(\\'r"
				+ optIndex.get(0)
				+ "\\').click();\">"
				+ opt[optIndex.get(0)]
				+ "</td></tr>"
				+ "<tr><td class=\"radio\"><input id=\"r"
				+ optIndex.get(1)
				+ "\" name=\"userAns\" value=\""
				+ optIndex.get(1)
				+ "\" type=\"radio\"></td><td class=\"option\" id=\"o"
				+ optIndex.get(1)
				+ "\" onclick=\"document.getElementById(\\'r"
				+ optIndex.get(1)
				+ "\\').click();\">"
				+ opt[optIndex.get(1)]
				+ "</td></tr>"
				+ "<tr><td class=\"radio\"><input id=\"r"
				+ optIndex.get(2)
				+ "\" name=\"userAns\" value=\""
				+ optIndex.get(2)
				+ "\" type=\"radio\"></td><td class=\"option\" id=\"o"
				+ optIndex.get(2)
				+ "\" onclick=\"document.getElementById(\\'r"
				+ optIndex.get(2)
				+ "\\').click();\">"
				+ opt[optIndex.get(2)]
				+ "</td></tr>"
				+ "<tr><td class=\"radio\"><input id=\"r"
				+ optIndex.get(3)
				+ "\" name=\"userAns\" value=\""
				+ optIndex.get(3)
				+ "\" type=\"radio\"></td><td class=\"option\" id=\"o"
				+ optIndex.get(3)
				+ "\" onclick=\"document.getElementById(\\'r"
				+ optIndex.get(3)
				+ "\\').click();\">"
				+ opt[optIndex.get(3)]
				+ "</td></tr>" + "</tbody></table>";

		//String formattedQues = current.getQues().replace("\\", "\\\\\\\\");
		//Toast.makeText(getApplicationContext(), current.getQues() , Toast.LENGTH_LONG).show();
		
		String base = Environment.getExternalStorageDirectory().getAbsolutePath().toString();
		Toast.makeText(getApplicationContext(), base , Toast.LENGTH_LONG).show();
		
		wv.loadUrl("javascript:function showAndroidToast(toast) {Android.showToast(toast);}console.log(\"Hello World\");");
		wv.loadUrl("javascript:document.getElementById('math').innerHTML='<div id=\"ques\">"
				+ current.getQues() + "</div><br>" + option + "';");
		wv.loadUrl("javascript:$('input[name=userAns]:radio').click(function() {var userAns = $(this).val(); Android.setUserAnswer(userAns);});");
		wv.loadUrl("javascript:MathJax.Hub.Queue(['Typeset',MathJax.Hub]);");
		String userAns = q.questions[currentQuesNo].getUserAnsCode();
		wv.loadUrl("javascript:$(\"#r" + userAns + "\").click();");
	}

	private String readFromFile(String file) {

		String ret = "";

		try {
			InputStream inputStream = getResources().getAssets().open(
					file + ".json");
			if (inputStream != null) {
				InputStreamReader inputStreamReader = new InputStreamReader(
						inputStream);
				BufferedReader bufferedReader = new BufferedReader(
						inputStreamReader);
				String receiveString = "";
				StringBuilder stringBuilder = new StringBuilder();

				while ((receiveString = bufferedReader.readLine()) != null) {
					stringBuilder.append(receiveString);
				}

				inputStream.close();
				ret = stringBuilder.toString();
			}
		} catch (FileNotFoundException e) {
			// Log.e(TAG, "File not found: " + e.toString());
		} catch (IOException e) {
			// Log.e(TAG, "Can not read file: " + e.toString());
		}

		return ret;
	}

	public Quiz parseJson(String json) throws JSONException {

		JSONObject jsonObject = jsonObject = new JSONObject(json);
		Quiz q = new Quiz();
		q.setId(jsonObject.getString("id"));
		q.setQuiz(jsonObject.getString("quiz"));
		JSONArray questions = jsonObject.getJSONArray("questions");
		Question[] quesArray = new Question[questions.length()];

		for (int i = 0; i < questions.length(); i++) {
			String ques = questions.getJSONObject(i).getString("q").toString();
			String opt = questions.getJSONObject(i).getString("o").toString();
			String fb = questions.getJSONObject(i).getString("f").toString();
			Question question = new Question(ques, opt, fb);
			quesArray[i] = question;
		}
		q.setQuestions(quesArray);
		return q;
	}

	private ArrayList<Integer> shuffle() {
		ArrayList<Integer> opt = new ArrayList<Integer>();
		opt.add(0);
		opt.add(1);
		opt.add(2);
		opt.add(3);
		Collections.shuffle(opt);
		return opt;
	}
	
	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}

}
