/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.courselib.quiz;

import android.app.Activity;
import android.app.ExpandableListActivity;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.ListView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.Toast;

import com.google.analytics.tracking.android.EasyTracker;
import com.myvision.courselib.R;
import com.myvision.courselib.db.DB1;
import com.myvision.courselib.db.Database;
import com.myvision.courselib.general.ChapterListArrayAdapter;
import com.myvision.courselib.general.QuizListArrayAdapter;
import com.myvision.courselib.ncert.NCERTExerciseProblemActivity;
import com.myvision.courselib.quiz.QuizExpandableAdapter;
import com.myvision.model.QuizData;

import java.util.ArrayList;

/**
 * 
 * @author ankit
 */
public class QuizListActivity extends  ListActivity {
	// Create ArrayList to hold parent Items and Child Items
	String chapterid;
	String chapter;

	ListView lv;
	Button back;

	static QuizData[] q;

	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);

		Intent i = getIntent();
		chapterid = i.getStringExtra("chapterid");
		chapter = i.getStringExtra("chapter");
		String temp = chapter.substring(chapter.indexOf(".") + 1);
		setTitle("CourseLib-Class 9 Mathematics " + temp + " Quiz List");

		q = Database.getQuizData(chapterid);

		String[] quizList = new String[q.length];
		for (int j = 0; j < quizList.length; j++) {
			quizList[j] = q[j].getQuiz();
		}
		setListAdapter(new QuizListArrayAdapter(this, quizList));

	}

	protected void onListItemClick(ListView l, View v, int position, long id) {

		String jsonFilename = q[position].getQuizjson();
		Intent i = new Intent(getApplicationContext(),
				QuizQuestionActivity.class);
		i.putExtra("jsonfile", jsonFilename + "");
		startActivity(i);
	}

	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.chapter, menu);
		return true;
	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}
	
}
