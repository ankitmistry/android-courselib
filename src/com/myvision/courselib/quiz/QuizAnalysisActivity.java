package com.myvision.courselib.quiz;

import org.achartengine.ChartFactory;
import org.achartengine.model.CategorySeries;
import org.achartengine.renderer.DefaultRenderer;
import org.achartengine.renderer.SimpleSeriesRenderer;

import com.google.analytics.tracking.android.EasyTracker;
import com.myvision.courselib.R;

import android.os.Bundle;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.view.Menu;
import android.widget.Toast;

public class QuizAnalysisActivity extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.quiz_analysis);
		Intent i = getIntent();
		String c1 = i.getStringExtra("correct");
		String w1 = i.getStringExtra("wrong");
		
		Integer c = Integer.parseInt(c1);
		Integer w = Integer.parseInt(w1);
		CreatePieChart(c1,w1);
	}

	private void CreatePieChart(String c1,String w1) {

		// Pie Chart Section Names
		String[] code = new String[] { "Wrong", "Right" };

		// Pie Chart Section Value
		double[] distribution = { Double.parseDouble(w1), Double.parseDouble(c1) };

		// Color of each Pie Chart Sections
		int[] colors = { Color.RED, Color.parseColor("#008000") };

		// Instantiating CategorySeries to plot Pie Chart
		CategorySeries distributionSeries = new CategorySeries(
				"Mobile Platforms");
		for (int i = 0; i < distribution.length; i++) {
			// Adding a slice with its values and name to the Pie Chart
			distributionSeries.add(code[i], distribution[i]);
		}
		// Instantiating a renderer for the Pie Chart
		DefaultRenderer defaultRenderer = new DefaultRenderer();
		for (int i = 0; i < distribution.length; i++) {
			SimpleSeriesRenderer seriesRenderer = new SimpleSeriesRenderer();
			seriesRenderer.setColor(colors[i]);
			seriesRenderer.setDisplayChartValues(true);
			// Adding a renderer for a slice
			defaultRenderer.addSeriesRenderer(seriesRenderer);
		}
		
		defaultRenderer.setLegendTextSize(30);
		defaultRenderer.setChartTitle("Quiz Report");
		defaultRenderer.setChartTitleTextSize(35);
		defaultRenderer.setLabelsColor(Color.parseColor("#000000"));
		defaultRenderer.setLabelsTextSize(30);
		defaultRenderer.setZoomButtonsVisible(true);
		defaultRenderer.setBackgroundColor(Color.parseColor("#FFFFFF"));

		// Creating an intent to plot bar chart using dataset and
		// multipleRenderer
		Intent intent = ChartFactory.getPieChartIntent(getBaseContext(),
				distributionSeries, defaultRenderer, "PieChart");

		// Start Activity
		startActivity(intent);

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.quiz_analysis, menu);
		return true;
	}
	
	
	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}

}
