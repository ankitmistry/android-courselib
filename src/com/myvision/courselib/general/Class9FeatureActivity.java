package com.myvision.courselib.general;

import com.google.analytics.tracking.android.EasyTracker;
import com.myvision.courselib.R;
import com.myvision.courselib.ncert.NCERTChapterActivity;
import com.myvision.courselib.ncert.NCERTExerciseProblemActivity;
import com.myvision.courselib.quiz.QuizChapterActivity;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.*;
import android.content.Intent;
import android.graphics.Color;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.TabHost;
import android.widget.TabHost.OnTabChangeListener;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.Toast;



@SuppressLint("NewApi")
public class Class9FeatureActivity extends TabActivity implements OnTabChangeListener{

	TabHost tabHost;
	TextView tv1;
	TextView tv2;
	
	String clicked = "#008b8b";
	//String unclicked = "#A63c00";
	String unclicked = "#FFFFFF";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		
		super.onCreate(savedInstanceState);
		setContentView(R.layout.class9feature);
		
		//getActionBar().setDisplayHomeAsUpEnabled(true);
        //getActionBar().setHomeButtonEnabled(true);
		
        
		tabHost = getTabHost();

		TabSpec ncertspec = tabHost.newTabSpec("NCERT Solution");
		ncertspec.setIndicator("NCERT Solution");
		Intent ncertIntent = new Intent(this, NCERTChapterActivity.class);
		ncertspec.setContent(ncertIntent);
		
		TabSpec quizspec = tabHost.newTabSpec("Quiz");
		quizspec.setIndicator("Quiz");
		Intent quizIntent = new Intent(this, QuizChapterActivity.class);
		quizspec.setContent(quizIntent);

		/*
		 * TabSpec lcspec = tabHost.newTabSpec("Learning Card");
		 * lcspec.setIndicator("Learning Card"); Intent lcIntent = new
		 * Intent(this, NCERTChapterActivity.class);
		 * lcspec.setContent(lcIntent);
		 */

		
		tabHost.addTab(ncertspec); // Adding ncert tab
		tabHost.addTab(quizspec); // Adding quiz tab
		// tabHost.addTab(lcspec); // Adding learning card tab
		
		tabHost.setOnTabChangedListener(this);

		tv1 = (TextView) tabHost.getTabWidget().getChildAt(0).findViewById(android.R.id.title);
		tv2 = (TextView) tabHost.getTabWidget().getChildAt(1).findViewById(android.R.id.title);
		tv1.setTextColor(Color.parseColor("#ffffff"));tv1.setTextSize(15);
		tv2.setTextColor(Color.parseColor("#000000"));tv2.setTextSize(15);
		tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor(clicked));
		tabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor(unclicked));
	}
	
	/*
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {

	    switch (item.getItemId()) {

	    case android.R.id.home:
	         Intent i = new Intent(getApplicationContext(), Home.class);
	         startActivity(i);
	    default:
	        return super.onOptionsItemSelected(item);
	    }
	}*/

	
	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}
	
	@Override
	public void onTabChanged(String tabId) {
		// TODO Auto-generated method stub
		//Toast.makeText(getApplicationContext(), tabHost.getCurrentTab()+"",
		//Toast.LENGTH_LONG).show();
		
		int currentTab = tabHost.getCurrentTab();
		if(currentTab == 0){
			tv1.setTextColor(Color.parseColor("#FFFFFF"));
			tv2.setTextColor(Color.parseColor("#000000"));
			tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor(clicked));
			tabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor(unclicked));
		}
		else{
			tv1.setTextColor(Color.parseColor("#000000"));
			tv2.setTextColor(Color.parseColor("#FFFFFF"));
			tabHost.getTabWidget().getChildAt(0).setBackgroundColor(Color.parseColor(unclicked));
			tabHost.getTabWidget().getChildAt(1).setBackgroundColor(Color.parseColor(clicked));
		}
	}
}