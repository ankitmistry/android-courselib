package com.myvision.courselib.general;

import com.myvision.courselib.quiz.QuizQuestionActivity;

import android.content.Context;
import android.widget.Toast;

public class JavaScriptWebAppInterface {
    Context mContext;

    /** Instantiate the interface and set the context */
    public JavaScriptWebAppInterface(Context c) {
        mContext = c;
    }

    /** Show a toast from the web page */
    //@JavascriptInterface
    public void showToast(String toast) {
        Toast.makeText(mContext, toast, Toast.LENGTH_SHORT).show();
    }
    
    
    /*public void checkAnswer(String userAns) {
    	if(QuizActivity.current.getCorrectAnsCode() == userAns){
    		Toast.makeText(mContext, "correct", Toast.LENGTH_SHORT).show();
    	}
    	else{
    		Toast.makeText(mContext, "wrong", Toast.LENGTH_SHORT).show();
    	}
    }*/
    
    
    
    public void setUserAnswer(String userAns){
    	QuizQuestionActivity.q.questions[QuizQuestionActivity.currentQuesNo].setUserAnsCode(userAns);
    	//Toast.makeText(mContext, userAns, Toast.LENGTH_LONG).show();
    }
}
