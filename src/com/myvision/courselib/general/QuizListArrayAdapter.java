package com.myvision.courselib.general;


import com.myvision.courselib.R;
import com.myvision.courselib.R.drawable;
import com.myvision.courselib.R.id;
import com.myvision.courselib.R.layout;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class QuizListArrayAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final String[] values;
 
	public QuizListArrayAdapter(Context context, String[] values) {
		super(context, R.layout.quiz_list1, values);
		this.context = context;
		this.values = values;
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View rowView = inflater.inflate(R.layout.quiz_list1, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.label);
		textView.setText(values[position]);
		textView.setTypeface(null,Typeface.BOLD);
		
		return rowView;
	}
}