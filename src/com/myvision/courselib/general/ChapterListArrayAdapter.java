package com.myvision.courselib.general;


import com.myvision.courselib.R;
import com.myvision.courselib.R.drawable;
import com.myvision.courselib.R.id;
import com.myvision.courselib.R.layout;

import android.content.Context;
import android.graphics.Typeface;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
 
public class ChapterListArrayAdapter extends ArrayAdapter<String> {
	private final Context context;
	private final String[] values;
 
	public ChapterListArrayAdapter(Context context, String[] values) {
		super(context, R.layout.chapterlist, values);
		this.context = context;
		this.values = values;
	}
 
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
			.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
 
		View rowView = inflater.inflate(R.layout.chapterlist, parent, false);
		TextView textView = (TextView) rowView.findViewById(R.id.label);
		ImageView imageView = (ImageView) rowView.findViewById(R.id.logo);
		textView.setText(values[position].substring(values[position].indexOf(".")+1));
		textView.setTypeface(null,Typeface.BOLD);
		
		String s = values[position];
  
		if (position == 0) {
			imageView.setImageResource(R.drawable.m1);
		} else if (position == 1) {
			imageView.setImageResource(R.drawable.m2);
		} else if (position == 2) {
			imageView.setImageResource(R.drawable.m3);
		} else if (position == 3) {
			imageView.setImageResource(R.drawable.m4);
		} else if (position == 4) {
			imageView.setImageResource(R.drawable.m5);
		} else if (position == 5) {
			imageView.setImageResource(R.drawable.m6);
		} else if (position == 6) {
			imageView.setImageResource(R.drawable.m7);
		} else if (position == 7) {
			imageView.setImageResource(R.drawable.m8);
		} else if (position == 8) {
			imageView.setImageResource(R.drawable.m9);
		} else if (position == 9) {
			imageView.setImageResource(R.drawable.m10);
		} else if (position == 10) {
			imageView.setImageResource(R.drawable.m6);
		} else if (position == 11) {
			imageView.setImageResource(R.drawable.m2);
		} else if (position == 12) {
			imageView.setImageResource(R.drawable.m3);
		} else if (position == 13) {
			imageView.setImageResource(R.drawable.m4);
		} else if (position == 14) {
			imageView.setImageResource(R.drawable.m5);
		} else if (position == 15) {
			imageView.setImageResource(R.drawable.m6);
		} else {
			imageView.setImageResource(R.drawable.m1);
		}
 
		return rowView;
	}
}