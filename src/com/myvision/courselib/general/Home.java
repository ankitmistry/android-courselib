package com.myvision.courselib.general;

import android.os.Bundle;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.Toast;
import java.io.IOException;
import java.io.InputStream;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.google.analytics.tracking.android.EasyTracker;
import com.myvision.courselib.R;

@SuppressLint("NewApi")
public class Home extends Activity {

	Button b1;
	Button b2;

	@SuppressLint("NewApi")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.home);

		b1 = (Button) findViewById(R.id.button1);
		b1.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				// TODO Auto-generated method stub
				Intent i = new Intent(getApplicationContext(),
						Class9FeatureActivity.class);
				i.putExtra("name", "Ankit");
				startActivity(i);

			}
		});

		/*
		 * b2 = (Button) findViewById(R.id.button2); b2.setOnClickListener(new
		 * OnClickListener() { public void onClick(View v) {
		 * 
		 * Toast.makeText(getApplicationContext(),"Coming Soon",
		 * Toast.LENGTH_LONG).show(); //Intent i = new
		 * Intent(getApplicationContext(), QuizActivity.class);
		 * //startActivity(i);
		 * 
		 * } });
		 */

	}

	@Override
	public void onStart() {
		super.onStart();
		EasyTracker.getInstance(this).activityStart(this); // Add this method.
	}

	@Override
	public void onStop() {
		super.onStop();
		EasyTracker.getInstance(this).activityStop(this); // Add this method.
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.grade, menu);
		return true;
	}
}
