package com.myvision.model;

public class QuizData {
	
	public String id;
	public String quiz;
	public String quizjson;
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getQuiz() {
		return quiz;
	}

	public void setQuiz(String quiz) {
		this.quiz = quiz;
	}

	public String getQuizjson() {
		return quizjson;
	}

	public void setQuizjson(String quizjson) {
		this.quizjson = quizjson;
	}

	public QuizData(String id, String quiz, String quizjson) {
		super();
		this.id = id;
		this.quiz = quiz;
		this.quizjson = quizjson;
	}
	
	

}
