package com.myvision.model;

import java.util.List;


public class Quiz {

	public String id;
	public String quiz;
	public Question[] questions;
	
	
	public Quiz(){
		
	}
	
	public Quiz(String id, String quiz, Question[] questions) {
		super();
		this.id = id;
		this.quiz = quiz;
		this.questions = questions;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getQuiz() {
		return quiz;
	}
	public void setQuiz(String quiz) {
		this.quiz = quiz;
	}
	public Question[] getQuestions() {
		return questions;
	}
	public void setQuestions(Question[] questions) {
		this.questions = questions;
	}
	
	
	
	
	
	
}
