/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.myvision.model;

/**
 *
 * @author ankit
 */
public class Problem {
    
    //public int problemId;
    public String problemNo;
    public String yId;
    
    

    /*public Problem(int problemId, int problemNo, String yId,Exercise exercise) {
        this.problemId = problemId;
        this.problemNo = problemNo;
        this.yId = yId;
        this.exercise = exercise;
    }*/

    // public Exercise exercise;
    public Problem(String problemNo, String yId) {
        this.problemNo = problemNo;
        this.yId = yId;
    }

    public String getProblemNo() {
        return problemNo;
    }

    public void setProblemNo(String problemNo) {
        this.problemNo = problemNo;
    }

    public String getyId() {
        return yId;
    }

    public void setyId(String yId) {
        this.yId = yId;
    }
    
    
    
    
}
