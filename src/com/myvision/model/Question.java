package com.myvision.model;

import java.security.PublicKey;
import java.util.ArrayList;

public class Question {

	public String ques;
	public String option;
	public String correctAns;
	public String feedback;
	public String correctAnsCode;
	public String userAnsCode;
	public ArrayList<Integer> optShuffleOrder;
	public boolean shuffled;
	
	public Question() {
		
	}
	
	public Question(String ques, String option, String feedback) {
		this.ques = ques;
		this.option = option;
		this.correctAns = option.split(",")[0];
		this.feedback = feedback;
		this.correctAnsCode = "0";
		this.userAnsCode = "-1";
		this.shuffled = false;
	}
	
	

	public ArrayList<Integer> getOptShuffleOrder() {
		return optShuffleOrder;
	}

	public void setOptShuffleOrder(ArrayList<Integer> optShuffleOrder) {
		this.optShuffleOrder = optShuffleOrder;
	}

	public boolean isShuffled() {
		return shuffled;
	}

	public void setShuffled(boolean shuffled) {
		this.shuffled = shuffled;
	}

	public String getCorrectAnsCode() {
		return correctAnsCode;
	}

	public void setCorrectAnsCode(String correctAnsCode) {
		this.correctAnsCode = correctAnsCode;
	}

	public String getUserAnsCode() {
		return userAnsCode;
	}

	public void setUserAnsCode(String userAnsCode) {
		this.userAnsCode = userAnsCode;
	}

	public String getQues() {
		return ques;
	}

	public void setQues(String ques) {
		this.ques = ques;
	}

	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

	public String getFeedback() {
		return feedback;
	}

	public void setFeedback(String feedback) {
		this.feedback = feedback;
	}

	public String getCorrectAns() {
		return correctAns;
	}

	public void setCorrectAns(String correctAns) {
		this.correctAns = correctAns;
	}
	
	
}
